package com.example;

import com.example.domain.Company;
import com.example.domain.CompanyDao;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {

    public static void main(String[] args) {

        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        CompanyDao dao=(CompanyDao) context.getBean("d");

        Company company = new Company();
        company.setName("Zara");
        company.setUnp(123456789);
        company.setPersonal(129);

        dao.saveCompany(company);
        dao.getCompany();
    }
}

