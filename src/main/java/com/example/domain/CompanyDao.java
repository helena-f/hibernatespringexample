package com.example.domain;

import org.springframework.orm.hibernate3.HibernateTemplate;

import java.util.ArrayList;
import java.util.List;

public class CompanyDao {

    HibernateTemplate template;

    public void setTemplate(HibernateTemplate template) {
        this.template = template;
    }

    public void saveCompany(Company company){
        template.save(company);
    }

    public List<Company> getCompany(){
        List<Company> list=new ArrayList<>();
        list=template.loadAll(Company.class);
        return list;
    }
}